<?php

declare(strict_types=1);

namespace Grifix\Money\Tests\Money;

use Grifix\Money\Currency\Currency;
use Grifix\Money\Money\Exceptions\CannotAddMoneyException;
use Grifix\Money\Money\Exceptions\CannotCreateMoneyException;
use Grifix\Money\Money\Exceptions\CannotSubtractMoneyException;
use Grifix\Money\Money\Exceptions\InvalidMoneyStringException;
use Grifix\Money\Money\Money;
use PHPUnit\Framework\TestCase;

final class MoneyTest extends TestCase
{

    public function testItCreates(): void
    {
        self::assertEquals('1.20 PLN', Money::pln(120)->toString());
        self::assertEquals('0.20 USD', Money::usd(20)->toString());
        self::assertEquals('1.00 EUR', Money::eur(100)->toString());
        self::assertEquals('501.01 GBP', Money::gbp(50101)->toString());
        self::assertEquals('141 JPY', Money::jpy(141)->toString());
        self::assertEquals('141 JPY', (string)Money::jpy(141));
    }

    /**
     * @dataProvider doesNotCreateDataProvider
     */
    public function testItDoesNotCreate(string $amount): void
    {
        $this->expectException(CannotCreateMoneyException::class);
        $this->expectExceptionMessage(sprintf('Cannot create money with amount [%s]!', $amount));
        Money::withCurrency($amount, 'PLN');
    }

    public function doesNotCreateDataProvider(): array
    {
        return [
            ['xxx'],
            ['1.20'],
            ['5_6'],
            ['x34'],
            ['20,50'],
            ['100USD']
        ];
    }

    public function testGetAmount(): void
    {
        $money = Money::withCurrency(100, 'PLN');
        self::assertEquals(100, $money->getAmount());
    }

    public function testGetCurrency(): void
    {
        $money = Money::withCurrency(100, 'USD');
        self::assertEquals(Currency::withCode('USD'), $money->getCurrency());
    }

    /**
     * @dataProvider itCreatesFromStringDataProvider
     */
    public function testItCreatesFromSting(string $value, Money $expectedMoney): void
    {
        self::assertEquals($expectedMoney, Money::createFromString($value));
    }

    public function itCreatesFromStringDataProvider(): array
    {
        return [
            [
                '100 USD',
                Money::usd(10000)
            ],
            [
                '100 JPY',
                Money::jpy(100)
            ],
            [
                '10.50 USD',
                Money::usd(1050)
            ],
            [
                '10.509 USD',
                Money::usd(1051)
            ],
            [
                '100.4 JPY',
                Money::jpy(100)
            ],
            [
                '100.6 JPY',
                Money::jpy(101)
            ],
            [
                '100.09 JPY',
                Money::jpy(100)
            ],
        ];
    }

    /**
     * @dataProvider itFailsToCreateFromInvalidStringProvider
     */
    public function testItFailsToCreateFromInvalidString(string $value): void
    {
        $this->expectException(InvalidMoneyStringException::class);
        $this->expectExceptionMessage('Money string must have format "INTEGER{.DECIMAL} CURRENCY" like "100.20 USD"');
        Money::createFromString($value);
    }

    public function itFailsToCreateFromInvalidStringProvider(): array
    {
        return [
            ['100'],
            ['USD'],
            ['100_USD'],
            ['100  USD']
        ];
    }

    public function testItAdds(): void
    {
        self::assertEquals(Money::usd(100), Money::usd(50)->add(Money::usd(50)));
    }

    public function testItFailsToAdd(): void
    {
        $this->expectException(CannotAddMoneyException::class);
        $this->expectExceptionMessage('Currencies must be identical');
        Money::usd(50)->add(Money::pln(50));
    }

    public function testItSubtracts(): void
    {
        self::assertEquals(Money::usd(-50), Money::usd(50)->subtract(Money::usd(100)));
    }

    public function testItFailsToSubtract(): void
    {
        $this->expectException(CannotSubtractMoneyException::class);
        $this->expectExceptionMessage('Currencies must be identical');
        Money::usd(50)->subtract(Money::pln(50));
    }
}
