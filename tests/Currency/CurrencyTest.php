<?php

declare(strict_types=1);

namespace Grifix\Money\Tests\Currency;

use Grifix\Money\Currency\Currency;
use Grifix\Money\Currency\Exceptions\CurrencyIsNotSupportedException;
use PHPUnit\Framework\TestCase;

final class CurrencyTest extends TestCase
{
    public function testItCreates(): void
    {
        self::assertEquals('PLN', Currency::pln()->getCode());
        self::assertEquals('USD', Currency::usd()->getCode());
        self::assertEquals('GBP', Currency::gbp()->getCode());
        self::assertEquals('JPY', Currency::jpy()->getCode());
        self::assertEquals('EUR', Currency::eur()->getCode());
        self::assertEquals('EUR', (string) Currency::eur());
    }

    /**
     * @dataProvider itDoesNotCreateDataProvider
     */
    public function testIdDoesNotCreate(string $currencyCode): void
    {
        $this->expectException(CurrencyIsNotSupportedException::class);
        $this->expectExceptionMessage(sprintf('Currency [%s] is not supported!', $currencyCode));
        Currency::withCode($currencyCode);
    }

    public function itDoesNotCreateDataProvider(): array
    {
        return [
            ['PLNX'],
            ['123'],
            ['pln']
        ];
    }

    public function testIsEqualTo(): void
    {
        self::assertTrue(Currency::usd()->isEqualTo(Currency::usd()));
        self::assertFalse(Currency::usd()->isEqualTo(Currency::gbp()));
    }
}
