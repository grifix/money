<?php

declare(strict_types=1);

namespace Grifix\Money\Currency\Exceptions;

final class CannotCreateCurrencyException extends \Exception
{
    public function __construct(string $code, \Throwable $previous)
    {
        parent::__construct(
            sprintf('Cannot create currency with code [%s]!', $code),
            0,
            $previous
        );
    }
}
