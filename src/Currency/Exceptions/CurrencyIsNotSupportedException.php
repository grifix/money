<?php

declare(strict_types=1);

namespace Grifix\Money\Currency\Exceptions;

final class CurrencyIsNotSupportedException extends \InvalidArgumentException
{
    public function __construct(string $currencyCode)
    {
        parent::__construct(sprintf('Currency [%s] is not supported!', $currencyCode));
    }
}
