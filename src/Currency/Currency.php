<?php

declare(strict_types=1);

namespace Grifix\Money\Currency;

use Grifix\Money\Currency\Exceptions\CannotCreateCurrencyException;
use Grifix\Money\Currency\Exceptions\CurrencyIsNotSupportedException;
use Money\Currency as PhpCurrency;

class Currency
{
    private PhpCurrency $value;

    private const USD = 'USD';
    private const PLN = 'PLN';
    private const EUR = 'EUR';
    private const GBP = 'GBP';
    private const JPY = 'JPY';

    private const SUPPORTED_CURRENCIES = [
        self::USD,
        self::PLN,
        self::EUR,
        self::GBP,
        self::JPY
    ];

    public function __construct(string $code)
    {
        $this->assertCurrencyIsSupported($code);
        try {
            $this->value = new PhpCurrency($code);
        } catch (\Throwable $exception) {
            throw new CannotCreateCurrencyException($code, $exception);
        }
    }

    public function getCode(): string
    {
        return $this->value->getCode();
    }

    public static function usd(): self
    {
        return new self(self::USD);
    }

    public static function pln(): self
    {
        return new self(self::PLN);
    }

    public static function eur(): self
    {
        return new self(self::EUR);
    }

    public static function gbp(): self
    {
        return new self(self::GBP);
    }

    public static function jpy(): self
    {
        return new self(self::JPY);
    }

    public static function withCode(string $code): self
    {
        return new self($code);
    }

    private function assertCurrencyIsSupported(string $currencyCode): void
    {
        if ( ! in_array($currencyCode, self::SUPPORTED_CURRENCIES)) {
            throw new CurrencyIsNotSupportedException($currencyCode);
        }
    }

    public static function getSupportedCurrencies(): array
    {
        return self::SUPPORTED_CURRENCIES;
    }

    public function isEqualTo(self $other): bool
    {
        return $this->getCode() === $other->getCode();
    }

    public function __toString(): string
    {
        return $this->getCode();
    }
}
