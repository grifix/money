<?php

declare(strict_types=1);

namespace Grifix\Money\Money;

use Grifix\Money\Currency\Currency;
use Grifix\Money\Money\Exceptions\CannotAddMoneyException;
use Grifix\Money\Money\Exceptions\CannotSubtractMoneyException;
use Grifix\Money\Money\Exceptions\CannotCreateMoneyException;
use Grifix\Money\Money\Exceptions\InvalidMoneyStringException;
use Money\Currencies\ISOCurrencies;
use Money\Currency as PhpCurrency;
use Money\Formatter\DecimalMoneyFormatter;
use Money\Money as PhpMoney;
use Money\Parser\DecimalMoneyParser;
use Throwable;

class Money
{
    private PhpMoney $amount;

    private Currency $currency;

    /**
     * @throws CannotCreateMoneyException
     */
    public function __construct(int|string $amount, Currency $currency)
    {
        try {
            $this->amount = new PhpMoney($amount, new PhpCurrency($currency->getCode()));
        } catch (\Exception $e) {
            throw new CannotCreateMoneyException($amount, $e);
        }
        $this->currency = $currency;
    }

    /**
     * @return numeric-string
     */
    public function getAmount(): string
    {
        return $this->amount->getAmount();
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     * @param numeric-string|int $amount
     *
     * @throws CannotCreateMoneyException
     */
    public static function usd(string|int $amount): self
    {
        return new self($amount, Currency::usd());
    }

    /**
     * @param numeric-string|int $amount
     *
     * @throws CannotCreateMoneyException
     */
    public static function gbp(string|int $amount): self
    {
        return new self($amount, Currency::gbp());
    }

    /**
     * @param numeric-string|int $amount
     *
     * @throws CannotCreateMoneyException
     */
    public static function pln(string|int $amount): self
    {
        return new self($amount, Currency::pln());
    }

    /**
     * @param numeric-string|int $amount
     *
     * @throws CannotCreateMoneyException
     */
    public static function eur(string|int $amount): self
    {
        return new self($amount, Currency::eur());
    }

    /**
     * @param numeric-string|int $amount
     *
     * @throws CannotCreateMoneyException
     */
    public static function jpy(string|int $amount): self
    {
        return new self($amount, Currency::jpy());
    }

    /**
     * @param numeric-string|int $amount
     *
     * @throws CannotCreateMoneyException
     */
    public static function withCurrency(string|int $amount, string $currencyCode): self
    {
        return new self($amount, Currency::withCode($currencyCode));
    }

    public function toString(): string
    {
        $amount = (new DecimalMoneyFormatter(new ISOCurrencies()))->format($this->amount);

        return $amount . ' ' . $this->currency->getCode();
    }

    public static function createFromString(string $value): self
    {
        $array = explode(' ', $value);
        if (empty($array[1])) {
            throw new InvalidMoneyStringException();
        }
        $amount = (new DecimalMoneyParser(new ISOCurrencies()))->parse($array[0], new PhpCurrency($array[1]));

        return self::withCurrency($amount->getAmount(), $array[1]);
    }

    public function add(self $other): self
    {
        try {
            $result = $this->amount->add($other->amount);
        } catch (Throwable $exception) {
            throw new CannotAddMoneyException($exception);
        }


        return self::withCurrency($result->getAmount(), $result->getCurrency()->getCode());
    }

    public function subtract(self $other): self{
        try {
            $result = $this->amount->subtract($other->amount);
        } catch (Throwable $exception) {
            throw new CannotSubtractMoneyException($exception);
        }

        return self::withCurrency($result->getAmount(), $result->getCurrency()->getCode());
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
