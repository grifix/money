<?php

declare(strict_types=1);

namespace Grifix\Money\Money\Exceptions;

use Exception;

final class InvalidMoneyStringException extends Exception
{
    public function __construct()
    {
        parent::__construct('Money string must have format "INTEGER{.DECIMAL} CURRENCY" like "100.20 USD"');
    }
}
