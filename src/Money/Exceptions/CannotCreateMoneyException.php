<?php

declare(strict_types=1);

namespace Grifix\Money\Money\Exceptions;

final class CannotCreateMoneyException extends \Exception
{
    /**
     * @param numeric-string|int $amount
     */
    public function __construct(string|int $amount, \Throwable $previous)
    {
        parent::__construct(
            sprintf('Cannot create money with amount [%s]!', $amount),
            0,
            $previous
        );
    }
}
