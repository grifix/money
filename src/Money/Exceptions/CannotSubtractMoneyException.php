<?php

declare(strict_types=1);

namespace Grifix\Money\Money\Exceptions;

use Throwable;

final class CannotSubtractMoneyException extends \Exception
{

    public function __construct(Throwable $previous)
    {
        parent::__construct(
            $previous->getMessage(),
            previous: $previous
        );
    }
}
